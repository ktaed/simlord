# -*- coding: utf-8 -*-

__version__ = "1.0.2"
__author__ = "Bianca Stöcker"

DESCRIPTION = "SimLoRD is a read simulator for long reads from third generation sequencing and is currently focused on the Pacific Biosciences SMRT error model."

